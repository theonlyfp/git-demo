Readme
=========


This is jekyl project.

Go [here](https://gitlab.com/theonlyfp/git-demo/blob/master/index.html) for details


A note about using this offline
--------------------------------

As root
1. Install bundler: `apt-get install ruby-bundler`
2. Install ruby headers: `apt-get install ruby-dev`
3. Install zlib devel files: `apt-get install zlib1g-dev`

As user
1. Git clone <repo>
2. cd git-demo
3. Run `bundle install --path .gems`

And then run the site using `bundle exec jekyll serve`
